const dataset = require("./dataset.json");

class MainController {
    constructor(app) {
        app.get("/", (req, res) => {
           res.render("table", {users: dataset})
        });
    }
}

module.exports = MainController;